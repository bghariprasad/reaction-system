import React from "react";
import "./App.scss";

import Reactions from "./Views/Reactions";

function App() {
  return (
    <section className="app">
      <div className="app-wrapper">
        <Reactions />
      </div>
    </section>
  );
}

export default App;
