export default class Service {
  static get(url, success) {
    fetch(url)
      .then(async (result) => {
        success(await result.json());
      })
      .catch((error) => console.error(error));
  }
}