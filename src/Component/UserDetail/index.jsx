import React, { Component } from "react";
import close from "../../Assets/close.svg";
import "./style.scss";

export default class UserDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedReactionId: props.selectedReactionId,
    };
  }

  componentDidMount() {
    this.props.selectUserByReaction(this.state.selectedReactionId);
  }

  handleTabSelection = (selectedReactionId) => {
    this.setState({ selectedReactionId }, () =>
      this.props.selectUserByReaction(this.state.selectedReactionId)
    );
  };

  renderUsers = () => {
    const { selectedUsers } = this.props;

    return (
      <div className="user-detail-container">
        {selectedUsers.map((user, index) => (
          <div key={index} className="user-detail-reactions">
            <img
              src={user.avatar}
              alt={`${user.first_name} ${user.last_name}`}
            />
            <div className="user-data">
              <span>
                {user.first_name} {user.last_name}
              </span>
              <span>{user.email}</span>
            </div>
          </div>
        ))}
      </div>
    );
  };

  render() {
    const { uniqueReactions, closeUserDialog } = this.props;
    const { selectedReactionId } = this.state;

    return (
      <div className="user-detail">
        <header className="header">
          <div className="header-option-container">
            <span
              onClick={() => this.handleTabSelection("all")}
              className={`header-option ${
                selectedReactionId === "all" && "active"
              }`}
            >
              All
            </span>
            {uniqueReactions.map((reaction) => (
              <span
                key={reaction.id}
                className={`header-option ${
                  selectedReactionId === reaction.id && "active"
                }`}
                onClick={() => this.handleTabSelection(reaction.id)}
              >
                {reaction.emoji}
              </span>
            ))}
          </div>
          <img
            className="close-icon"
            src={close}
            alt="close"
            onClick={() => closeUserDialog("all", false)}
          />
        </header>
        {this.renderUsers()}
      </div>
    );
  }
}
