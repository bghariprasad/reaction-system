import React, { PureComponent } from "react";

import "./style.scss";

export default class Summary extends PureComponent {
  state = {
    contentSummary: [],
    uniqueReactions: [],
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.summaries !== this.props.summaries ||
      prevProps.reactions !== this.props.reactions
    ) {
      this.getSummaryByContent(2);
    }
  }

  getSummaryByContent = (contentId) => {
    const { summaries, reactions } = this.props;
    const uniqueReaction = {};
    const contentSummary = summaries.filter((summary) => {
      if (summary.content_id === contentId) {
        uniqueReaction[summary.reaction_id] = true;
        return true;
      }
      return false;
    });
    const uniqueReactions = reactions.filter((reaction) =>
      Object.keys(uniqueReaction).includes(String(reaction.id))
    );
    this.setState(
      {
        contentSummary,
        uniqueReactions,
      },
      () => this.props.setUniqueReaction(this.state.uniqueReactions)
    );
  };

  renderUsersByReaction = () => {
    const { selectedUsers } = this.props;
    const MORE_CONST = 5;
    const moreUsers = selectedUsers.length - MORE_CONST;
    return (
      <div className="tooltip">
        {selectedUsers.slice(0, MORE_CONST).map((user, index) => (
          <span key={index}>
            {user.first_name} {user.last_name}
          </span>
        ))}
        {moreUsers > 0 && <span>and {moreUsers} more...</span>}
      </div>
    );
  };

  render() {
    const { uniqueReactions, contentSummary } = this.state;
    const { selectUserByReaction, openUserDialog, isReacted } = this.props;

    return (
      <>
        <section className="reaction-summary">
          <div className="emoji-container">
            {uniqueReactions.slice(0, 3).map((reaction) => (
              <span
                key={reaction.id}
                onClick={() => openUserDialog(reaction.id)}
                onMouseOver={() => selectUserByReaction(reaction.id)}
                className="summary-emoji"
              >
                {reaction.emoji}
              </span>
            ))}
            {this.renderUsersByReaction()}
          </div>
          <span
            className="count"
            onClick={() => openUserDialog()}
            onMouseOver={() => selectUserByReaction()}
          >
            {!!contentSummary.length &&
              (isReacted
                ? `You and ${contentSummary.length}`
                : contentSummary.length)}
            {this.renderUsersByReaction()}
          </span>
        </section>
      </>
    );
  }
}
