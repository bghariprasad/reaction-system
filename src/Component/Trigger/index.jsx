import React, { PureComponent } from "react";
import Like from "../../Assets/like.svg";

import "./style.scss";

export default class Trigger extends PureComponent {
  state = {
    isTriggerVisible: false,
    selectedReaction: {},
  };

  handleTrigger = () => {
    const { isTriggerVisible } = this.state;
    this.setState({
      isTriggerVisible: !isTriggerVisible,
      selectedReaction: {},
    });
    this.props.manageUser();
  };

  handleReaction = (reaction) => {
    this.setState({ selectedReaction: reaction, isTriggerVisible: false });
    this.props.manageUser(reaction);
  };

  render() {
    const {
      isTriggerVisible,
      selectedReaction: { name, emoji },
    } = this.state;

    return (
      <div className="trigger">
        {isTriggerVisible && (
          <div className="trigger-container">
            {this.props.reactions.map((reaction) => (
              <div key={reaction.id} className="emoji-container">
                <span className="reaction-tooltip">{reaction.name}</span>
                <span
                  className="reaction-emoji"
                  onClick={() => this.handleReaction(reaction)}
                >
                  {reaction.emoji}
                </span>
              </div>
            ))}
          </div>
        )}
        <div className="reaction-btn-container" onClick={this.handleTrigger}>
          <span className="reaction-btn">
            {name ? (
              <span>
                {emoji} <span className="active-like">{name}</span>
              </span>
            ) : (
              <span className="like-icon-container">
                <img className="like-icon" src={Like} alt="like" />
                Like
              </span>
            )}
          </span>
        </div>
      </div>
    );
  }
}
