import React, { PureComponent } from "react";
import "./style.scss";
import Service from "../../Utils/service";

import Trigger from "../../Component/Trigger";
import Summary from "../../Component/Summary";
import UserDetail from "../../Component/UserDetail";

export default class Reactions extends PureComponent {
  state = {
    reactions: [],
    summaries: [],
    users: [],
    uniqueReactions: [],
    selectedUsers: [],
    showUserDialog: false,
    selectedReactionId: "all",
    isReacted: false,
  };

  componentDidMount() {
    Service.get(
      "https://my-json-server.typicode.com/artfuldev/json-db-data/reactions",
      (reactions) => this.setState({ reactions })
    );
    this.getSummary();
    this.getUser();
  }

  getSummary() {
    Service.get(
      "https://my-json-server.typicode.com/artfuldev/json-db-data/user_content_reactions",
      (summaries) => this.setState({ summaries })
    );
  }

  getUser() {
    Service.get(
      "https://my-json-server.typicode.com/artfuldev/json-db-data/users",
      (users) => this.setState({ users })
    );
  }

  manageUser = (reaction) => {
    this.setState({ isReacted: reaction ? true : false });
  };

  setUniqueReaction = (uniqueReactions) => {
    this.setState({ uniqueReactions });
  };

  selectUserByReaction = (reactionId = "all") => {
    const { summaries, users } = this.state;
    const selectedUsers = [];
    summaries.forEach((summary) => {
      if (summary.reaction_id === reactionId || reactionId === "all") {
        selectedUsers.push(users.find((user) => user.id === summary.user_id));
      }
    });
    this.setState({ selectedUsers });
  };

  toggleUserDialog = (reactionId = "all", showUserDialog = true) => {
    this.setState({ showUserDialog, selectedReactionId: reactionId });
  };

  render() {
    const { showUserDialog } = this.state;

    return (
      <section className="reaction-container">
        <Summary
          {...this.state}
          setUniqueReaction={this.setUniqueReaction}
          selectUserByReaction={this.selectUserByReaction}
          openUserDialog={this.toggleUserDialog}
        />
        <Trigger
          reactions={this.state.reactions}
          manageUser={this.manageUser}
        />
        {showUserDialog && (
          <UserDetail
            {...this.state}
            selectUserByReaction={this.selectUserByReaction}
            closeUserDialog={this.toggleUserDialog}
          />
        )}
      </section>
    );
  }
}
